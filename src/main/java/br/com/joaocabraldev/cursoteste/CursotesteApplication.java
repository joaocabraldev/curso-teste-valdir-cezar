package br.com.joaocabraldev.cursoteste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursotesteApplication {

    public static void main(final String[] args) {
        SpringApplication.run(CursotesteApplication.class, args);
    }

}
