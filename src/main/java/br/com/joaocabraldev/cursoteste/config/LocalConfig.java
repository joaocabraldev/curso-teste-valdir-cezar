package br.com.joaocabraldev.cursoteste.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.joaocabraldev.cursoteste.user.User;
import br.com.joaocabraldev.cursoteste.user.UserRepository;

@Configuration
@Profile("local")
public class LocalConfig {

    @Autowired
    private UserRepository repository;

    @Bean
    public void initDb() {
        this.repository.saveAll(Arrays.asList(
                new User(null, "José da Silva", "123", "jose@gmail.com"),
                new User(null, "Maria Ferreira", "123", "maria@gmail.com")));
    }

}
