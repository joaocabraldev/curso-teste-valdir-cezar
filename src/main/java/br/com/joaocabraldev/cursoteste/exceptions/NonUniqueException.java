package br.com.joaocabraldev.cursoteste.exceptions;

public class NonUniqueException extends RuntimeException {

    public NonUniqueException(final String message) {
        super(message);
    }

}
