package br.com.joaocabraldev.cursoteste.exceptions;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<StandardError> notFoundExceptionHandler(final NotFoundException ex,
            final HttpServletRequest request) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(StandardError.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.NOT_FOUND.value())
                .error(ex.getMessage())
                .path(request.getRequestURI())
                .build());

    }

    @ExceptionHandler(NonUniqueException.class)
    public ResponseEntity<StandardError> nonUniqueExceptionHandler(final NonUniqueException ex,
            final HttpServletRequest request) {

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(StandardError.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.BAD_REQUEST.value())
                .error(ex.getMessage())
                .path(request.getRequestURI())
                .build());

    }

}
