package br.com.joaocabraldev.cursoteste.exceptions;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class StandardError {

    private final LocalDateTime timestamp;
    private final Integer status;
    private final String error;
    private final String path;

}
