package br.com.joaocabraldev.cursoteste.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    boolean existsByEmailAndIdNot(String email, Integer id);

}
