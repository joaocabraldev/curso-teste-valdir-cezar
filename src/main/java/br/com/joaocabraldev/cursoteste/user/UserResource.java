package br.com.joaocabraldev.cursoteste.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserResource {

    private static final String ID = "/{id}";

    @Autowired
    private UserService service;

    @GetMapping(UserResource.ID)
    public UserDTO findById(@PathVariable final Integer id) {
        return this.service.findById(id);
    }

    @GetMapping
    public List<UserDTO> findAll() {
        return this.service.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Integer create(@RequestBody final UserDTO dto) {
        return this.service.create(dto);
    }

    @PutMapping(UserResource.ID)
    @ResponseStatus(code = HttpStatus.CREATED)
    public void update(@PathVariable final Integer id, @RequestBody final UserDTO dto) {
        this.service.update(id, dto);
    }

    @DeleteMapping(UserResource.ID)
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable final Integer id) {
        this.service.delete(id);
    }

}
