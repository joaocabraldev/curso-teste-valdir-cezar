package br.com.joaocabraldev.cursoteste.user;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.joaocabraldev.cursoteste.exceptions.NonUniqueException;
import br.com.joaocabraldev.cursoteste.exceptions.NotFoundException;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private ModelMapper mapper;

    public UserDTO findById(final Integer id) {
        return this.mapper.map(this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Usuário não encontrado")), UserDTO.class);
    }

    public List<UserDTO> findAll() {
        return this.repository.findAll().stream().map(o -> this.mapper.map(o, UserDTO.class))
                .collect(Collectors.toList());
    }

    public Integer create(final UserDTO dto) {
        this.validateUserByEmail(dto);
        return this.repository.save(this.mapper.map(dto, User.class)).getId();
    }

    public void update(final Integer id, final UserDTO dto) {
        dto.setId(id);
        this.validateUserByEmail(dto);
        this.repository.save(this.mapper.map(dto, User.class));
    }

    public void delete(final Integer id) {
        try {
            this.repository.deleteById(id);
        } catch (final EmptyResultDataAccessException e) {
            throw new NotFoundException("Usuário não encontrado");
        }
    }

    private void validateUserByEmail(final UserDTO dto) {
        if (this.repository.existsByEmailAndIdNot(dto.getEmail(), dto.getId())) {
            throw new NonUniqueException("Já existe usuário com este e-mail");
        }
    }

}
