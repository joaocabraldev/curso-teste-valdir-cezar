package br.com.joaocabraldev.cursoteste.cursoteste;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.joaocabraldev.cursoteste.CursotesteApplication;

@SpringBootTest
class CursotesteApplicationTests {

    @Test
    void main() {
        CursotesteApplication.main(new String[] {});
        final Integer expected = 1;
        final Integer actual = 1;
        Assertions.assertEquals(expected, actual);
    }

}
