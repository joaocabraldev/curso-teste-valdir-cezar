package br.com.joaocabraldev.cursoteste.exceptions;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

@SpringBootTest
public class ResourceExceptionHandlerTest {

    private static final String OBJETO_NAO_ENCONTRADO = "Objeto não encontrado";
    private static final String OBJETO_JA_EXISTE = "Objeto já existe";

    @Autowired
    private ResourceExceptionHandler underTest;

    @Test
    void whenNonUniqueExceptionThenReturnACustomResponseEntity() {
        final ResponseEntity<StandardError> actual = this.underTest.nonUniqueExceptionHandler(
                new NonUniqueException(ResourceExceptionHandlerTest.OBJETO_JA_EXISTE),
                new MockHttpServletRequest());

        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.getBody());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, actual.getStatusCode());
        Assertions.assertEquals(ResponseEntity.class, actual.getClass());

        Assertions.assertEquals(StandardError.class, Optional.ofNullable(actual.getBody()).get().getClass());
        Assertions.assertEquals(ResourceExceptionHandlerTest.OBJETO_JA_EXISTE,
                Optional.ofNullable(actual.getBody()).get().getError());
        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(),
                Optional.ofNullable(actual.getBody()).get().getStatus());

        Assertions.assertNotEquals("/user/1", Optional.ofNullable(actual.getBody()).get().getPath());
        Assertions.assertNotEquals(LocalDateTime.now(), Optional.ofNullable(actual.getBody()).get().getTimestamp());
    }

    @Test
    void whenNotFoundExceptionThenReturnACustomResponseEntity() {
        final ResponseEntity<StandardError> actual = this.underTest.notFoundExceptionHandler(
                new NotFoundException(ResourceExceptionHandlerTest.OBJETO_NAO_ENCONTRADO),
                new MockHttpServletRequest());

        Assertions.assertNotNull(actual);
        Assertions.assertNotNull(actual.getBody());
        Assertions.assertEquals(HttpStatus.NOT_FOUND, actual.getStatusCode());
        Assertions.assertEquals(ResponseEntity.class, actual.getClass());

        Assertions.assertEquals(StandardError.class, Optional.ofNullable(actual.getBody()).get().getClass());
        Assertions.assertEquals(ResourceExceptionHandlerTest.OBJETO_NAO_ENCONTRADO,
                Optional.ofNullable(actual.getBody()).get().getError());
        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(),
                Optional.ofNullable(actual.getBody()).get().getStatus());
    }
}
