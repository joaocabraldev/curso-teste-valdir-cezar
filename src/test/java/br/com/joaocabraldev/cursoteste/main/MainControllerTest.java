package br.com.joaocabraldev.cursoteste.main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MainControllerTest {

    @Autowired
    private MainController underTest;

    @Test
    void testIndex() {
        final String expected = "Projeto Funcionando!";
        final String actual = this.underTest.index();
        Assertions.assertEquals(expected, actual);
    }
}
