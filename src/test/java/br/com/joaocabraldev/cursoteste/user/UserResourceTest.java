package br.com.joaocabraldev.cursoteste.user;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.joaocabraldev.cursoteste.exceptions.NonUniqueException;
import br.com.joaocabraldev.cursoteste.exceptions.NotFoundException;

@SpringBootTest
public class UserResourceTest {

    private static final String MENSAGEM_JA_EXISTE_USUARIO = "Já existe usuário com este e-mail";
    private static final String MENSAGEM_USUARIO_NAO_ENCONTRADO = "Usuário não encontrado";

    private static final int ONE_CALL = 1;
    private static final int FIRST_INDEX = 0;

    private static final int ID = 1;
    private static final String NAME = "José da Silva";
    private static final String EMAIL = "jose@gmail.com";
    private static final String PASSWORD = "123";

    @InjectMocks
    private UserResource underTest;

    @Mock
    private UserService service;

    private UserDTO dto;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        this.dto = new UserDTO(UserResourceTest.ID, UserResourceTest.NAME, UserResourceTest.EMAIL,
                UserResourceTest.PASSWORD);
    }

    @Test
    void whenFindByIdThenSuccess() {
        Mockito.when(this.service.findById(ArgumentMatchers.anyInt())).thenReturn(this.dto);

        final UserDTO actual = this.underTest.findById(UserResourceTest.ID);

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(UserDTO.class, actual.getClass());
        Assertions.assertEquals(UserResourceTest.ID, actual.getId());
        Assertions.assertEquals(UserResourceTest.NAME, actual.getName());
        Assertions.assertEquals(UserResourceTest.EMAIL, actual.getEmail());
        Assertions.assertEquals(UserResourceTest.PASSWORD, actual.getPassword());
    }

    @Test
    void whenFindByIdThenReturnNotFoundException() {
        Mockito.when(this.service.findById(ArgumentMatchers.anyInt()))
                .thenThrow(new NotFoundException(UserResourceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO));

        try {
            this.underTest.findById(UserResourceTest.ID);
        } catch (final NotFoundException actual) {
            Assertions.assertEquals(NotFoundException.class, actual.getClass());
            Assertions.assertEquals(UserResourceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO, actual.getMessage());
        }
    }

    @Test
    void whenFindAllThenReturnAListOfUserDTO() {
        Mockito.when(this.service.findAll()).thenReturn(List.of(this.dto));

        final List<UserDTO> actual = this.underTest.findAll();

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(1, actual.size());
        Assertions.assertEquals(UserDTO.class, actual.get(UserResourceTest.FIRST_INDEX).getClass());

        Assertions.assertEquals(UserResourceTest.ID, actual.get(UserResourceTest.FIRST_INDEX).getId());
        Assertions.assertEquals(UserResourceTest.NAME, actual.get(UserResourceTest.FIRST_INDEX).getName());
        Assertions.assertEquals(UserResourceTest.EMAIL, actual.get(UserResourceTest.FIRST_INDEX).getEmail());
        Assertions.assertEquals(UserResourceTest.PASSWORD, actual.get(UserResourceTest.FIRST_INDEX).getPassword());
    }

    @Test
    void whenCreateThenReturnUserId() {
        Mockito.when(this.service.create(ArgumentMatchers.any())).thenReturn(UserResourceTest.ID);

        final Integer actual = this.underTest.create(this.dto);

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(Integer.class, actual.getClass());
        Assertions.assertEquals(UserResourceTest.ID, actual);

    }

    @Test
    void whenCreateThenThrowsNonUniqueException() {
        Mockito.when(this.service.create(ArgumentMatchers.any()))
                .thenThrow(new NonUniqueException(UserResourceTest.MENSAGEM_JA_EXISTE_USUARIO));

        try {
            this.underTest.create(this.dto);
        } catch (final NonUniqueException ex) {
            Assertions.assertEquals(NonUniqueException.class, ex.getClass());
            Assertions.assertEquals(UserResourceTest.MENSAGEM_JA_EXISTE_USUARIO, ex.getMessage());
        }

    }

    @Test
    void whenUpdateThenSuccess() {
        Mockito.doNothing().when(this.service).update(ArgumentMatchers.anyInt(), ArgumentMatchers.any());

        this.underTest.update(UserResourceTest.ID, this.dto);

        Mockito.verify(this.service, Mockito.times(UserResourceTest.ONE_CALL)).update(UserResourceTest.ID, this.dto);
    }

    @Test
    void whenUpdateThenThrowsNonUniqueException() {
        Mockito.doThrow(new NonUniqueException(UserResourceTest.MENSAGEM_JA_EXISTE_USUARIO)).when(this.service)
                .update(ArgumentMatchers.anyInt(), ArgumentMatchers.any());

        try {
            this.underTest.update(UserResourceTest.ID, this.dto);
        } catch (final NonUniqueException ex) {
            Assertions.assertEquals(NonUniqueException.class, ex.getClass());
            Assertions.assertEquals(UserResourceTest.MENSAGEM_JA_EXISTE_USUARIO, ex.getMessage());
            Mockito.verify(this.service, Mockito.times(UserResourceTest.ONE_CALL)).update(UserResourceTest.ID,
                    this.dto);
        }

    }

    @Test
    void whenDeleteSuccess() {
        Mockito.doNothing().when(this.service).delete(ArgumentMatchers.anyInt());

        this.underTest.delete(UserResourceTest.ID);

        Mockito.verify(this.service, Mockito.times(UserResourceTest.ONE_CALL)).delete(UserResourceTest.ID);
    }

    @Test
    void whenDeleteThrowNotFoundException() {
        Mockito.doThrow(new NotFoundException(UserResourceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO)).when(this.service)
                .delete(ArgumentMatchers.anyInt());

        try {
            this.underTest.delete(UserResourceTest.ID);
        } catch (final NotFoundException ex) {
            Assertions.assertEquals(NotFoundException.class, ex.getClass());
            Assertions.assertEquals(UserResourceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO, ex.getMessage());
            Mockito.verify(this.service, Mockito.times(UserResourceTest.ONE_CALL)).delete(ArgumentMatchers.anyInt());
        }
    }

}
