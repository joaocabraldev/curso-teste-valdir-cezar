package br.com.joaocabraldev.cursoteste.user;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;

import br.com.joaocabraldev.cursoteste.exceptions.NonUniqueException;
import br.com.joaocabraldev.cursoteste.exceptions.NotFoundException;

@SpringBootTest
public class UserServiceTest {

    private static final String MENSAGEM_JA_EXISTE_USUARIO = "Já existe usuário com este e-mail";
    private static final String MENSAGEM_USUARIO_NAO_ENCONTRADO = "Usuário não encontrado";

    private static final int ONE_CALL = 1;
    private static final int NO_CALL = 0;
    private static final int FIRST_INDEX = 0;

    private static final int ID = 1;
    private static final String NAME = "José da Silva";
    private static final String EMAIL = "jose@gmail.com";
    private static final String PASSWORD = "123";

    @InjectMocks
    private UserService underTest;

    @Mock
    private UserRepository repository;

    @Mock
    private ModelMapper mapper;

    private UserDTO dto;

    private User object;

    private Optional<User> optionalObject;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        this.dto = new UserDTO(UserServiceTest.ID, UserServiceTest.NAME, UserServiceTest.EMAIL,
                UserServiceTest.PASSWORD);
        this.object = new User(UserServiceTest.ID, UserServiceTest.NAME, UserServiceTest.EMAIL,
                UserServiceTest.PASSWORD);
        this.optionalObject = Optional.of(this.object);
    }

    @Test
    void whenFindByIdThenReturnUserDTO() {
        Mockito.when(this.repository.findById(Mockito.anyInt())).thenReturn(this.optionalObject);
        Mockito.when(this.mapper.map(Mockito.any(), Mockito.any())).thenReturn(this.dto);

        final UserDTO actual = this.underTest.findById(UserServiceTest.ID);

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(UserDTO.class, actual.getClass());
        Assertions.assertEquals(UserServiceTest.ID, actual.getId());
        Assertions.assertEquals(UserServiceTest.NAME, actual.getName());
        Assertions.assertEquals(UserServiceTest.EMAIL, actual.getEmail());
        Assertions.assertEquals(UserServiceTest.PASSWORD, actual.getPassword());
    }

    @Test
    void whenFindByIdThenReturnNotFoundException() {
        Mockito.when(this.repository.findById(Mockito.anyInt()))
                .thenThrow(new NotFoundException(UserServiceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO));
        Mockito.when(this.mapper.map(Mockito.any(), Mockito.any())).thenReturn(this.dto);

        try {
            this.underTest.findById(UserServiceTest.ID);
        } catch (final NotFoundException actual) {
            Assertions.assertEquals(NotFoundException.class, actual.getClass());
            Assertions.assertEquals(UserServiceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO, actual.getMessage());
        }
    }

    @Test
    void whenFindAllTHenReturnAListOfUserDTO() {
        Mockito.when(this.repository.findAll()).thenReturn(List.of(this.object));
        Mockito.when(this.mapper.map(Mockito.any(), Mockito.any())).thenReturn(this.dto);

        final List<UserDTO> actual = this.underTest.findAll();

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(1, actual.size());
        Assertions.assertEquals(UserDTO.class, actual.get(UserServiceTest.FIRST_INDEX).getClass());

        Assertions.assertEquals(UserServiceTest.ID, actual.get(UserServiceTest.FIRST_INDEX).getId());
        Assertions.assertEquals(UserServiceTest.NAME, actual.get(UserServiceTest.FIRST_INDEX).getName());
        Assertions.assertEquals(UserServiceTest.EMAIL, actual.get(UserServiceTest.FIRST_INDEX).getEmail());
        Assertions.assertEquals(UserServiceTest.PASSWORD, actual.get(UserServiceTest.FIRST_INDEX).getPassword());
    }

    @Test
    void whenCreateThenReturnSuccess() {
        Mockito.when(this.repository.existsByEmailAndIdNot(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(false);
        Mockito.when(this.repository.save(ArgumentMatchers.any())).thenReturn(this.object);

        final Integer actual = this.underTest.create(this.dto);

        Assertions.assertNotNull(actual);
        Assertions.assertEquals(Integer.class, actual.getClass());
        Assertions.assertEquals(UserServiceTest.ID, actual);
    }

    @Test
    void whenCreateThenThrowsNonUniqueException() {
        Mockito.when(this.repository.existsByEmailAndIdNot(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(true);

        try {
            this.underTest.create(this.dto);
        } catch (final NonUniqueException ex) {
            Assertions.assertEquals(NonUniqueException.class, ex.getClass());
            Assertions.assertEquals(UserServiceTest.MENSAGEM_JA_EXISTE_USUARIO, ex.getMessage());
        }

    }

    @Test
    void whenUpdateThenSuccess() {
        Mockito.when(this.repository.existsByEmailAndIdNot(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(false);
        Mockito.when(this.mapper.map(Mockito.any(), Mockito.any())).thenReturn(this.object);
        Mockito.when(this.repository.save(ArgumentMatchers.any())).thenReturn(this.object);

        this.underTest.update(UserServiceTest.ID, this.dto);

        Mockito.verify(this.repository, Mockito.times(UserServiceTest.ONE_CALL)).existsByEmailAndIdNot(
                UserServiceTest.EMAIL,
                UserServiceTest.ID);
        Mockito.verify(this.mapper, Mockito.times(UserServiceTest.ONE_CALL)).map(this.dto, User.class);
        Mockito.verify(this.repository, Mockito.times(UserServiceTest.ONE_CALL)).save(this.object);
    }

    @Test
    void whenUpdateThenThrowsNonUniqueException() {
        Mockito.when(this.repository.existsByEmailAndIdNot(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(true);
        Mockito.when(this.mapper.map(Mockito.any(), Mockito.any())).thenReturn(this.object);
        Mockito.when(this.repository.save(ArgumentMatchers.any())).thenReturn(this.object);

        try {
            this.underTest.update(UserServiceTest.ID, this.dto);
        } catch (final NonUniqueException ex) {
            Mockito.verify(this.repository, Mockito.times(UserServiceTest.ONE_CALL)).existsByEmailAndIdNot(
                    UserServiceTest.EMAIL,
                    UserServiceTest.ID);
            Mockito.verify(this.mapper, Mockito.times(UserServiceTest.NO_CALL)).map(this.dto, User.class);
            Mockito.verify(this.repository, Mockito.times(UserServiceTest.NO_CALL)).save(this.object);

            Assertions.assertEquals(NonUniqueException.class, ex.getClass());
            Assertions.assertEquals(UserServiceTest.MENSAGEM_JA_EXISTE_USUARIO, ex.getMessage());
        }

    }

    @Test
    void whenDeleteThenSuccess() {
        Mockito.when(this.repository.findById(ArgumentMatchers.anyInt())).thenReturn(this.optionalObject);
        Mockito.doNothing().when(this.repository).deleteById(ArgumentMatchers.anyInt());
        this.underTest.delete(UserServiceTest.ID);
        Mockito.verify(this.repository, Mockito.times(UserServiceTest.ONE_CALL)).deleteById(ArgumentMatchers.anyInt());
    }

    @Test
    void whenDeleteThenThrowNotFoundException() {
        Mockito.when(this.repository.findById(ArgumentMatchers.anyInt()))
                .thenThrow(new EmptyResultDataAccessException(1));

        try {
            this.underTest.delete(UserServiceTest.ID);
        } catch (final NotFoundException ex) {
            Assertions.assertEquals(NotFoundException.class, ex.getClass());
            Assertions.assertEquals(UserServiceTest.MENSAGEM_USUARIO_NAO_ENCONTRADO, ex.getMessage());
            Mockito.verify(this.repository, Mockito.times(UserServiceTest.NO_CALL))
                    .deleteById(ArgumentMatchers.anyInt());
        }

    }

}
