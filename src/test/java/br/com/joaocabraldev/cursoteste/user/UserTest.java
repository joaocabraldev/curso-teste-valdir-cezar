package br.com.joaocabraldev.cursoteste.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {

    private User underTest;

    @BeforeEach
    void setup() {
        this.underTest = new User();
    }

    @Test
    void testGetEmail() {
        final String expected = "joao@gmail.com";
        this.underTest.setEmail(expected);
        final String actual = this.underTest.getEmail();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testGetId() {
        final Integer expected = 1;
        this.underTest.setId(expected);
        final Integer actual = this.underTest.getId();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testGetName() {
        final String expected = "João";
        this.underTest.setName(expected);
        final String actual = this.underTest.getName();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testGetPassword() {
        final String expected = "123";
        this.underTest.setPassword(expected);
        final String actual = this.underTest.getPassword();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testSetEmail() {
        final String expected = "joao@gmail.com";
        this.underTest.setEmail(expected);
        final String actual = this.underTest.getEmail();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testSetId() {
        final Integer expected = 1;
        this.underTest.setId(expected);
        final Integer actual = this.underTest.getId();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testSetName() {
        final String expected = "João";
        this.underTest.setName(expected);
        final String actual = this.underTest.getName();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testSetPassword() {
        final String expected = "123";
        this.underTest.setPassword(expected);
        final String actual = this.underTest.getPassword();

        Assertions.assertEquals(expected, actual);
    }
}
